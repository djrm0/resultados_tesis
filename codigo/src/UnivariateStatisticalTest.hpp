/**
 * Author: Daniel J. Ramirez <djrmuv@gmail.com>
 */
#ifndef UNIVARIATE_STATISTICAL_TEST_H
#define UNIVARIATE_STATISTICAL_TEST_H

#include <vector>
#include <limits>

#include "element_t.hpp"


/** almacenamiento de resultados para una prueba de discordancia con valores contaminados */
class ContaminedTestResult
{
  public:
    // asigna el valor critico objetivo.
    virtual void set_target(double critical_target) = 0;
    // este metodo es ejecutado despues de una prueba para muestra contaminada
    // x almacena los valores que fueron evaluados por la prueba
    virtual void process(std::vector<element_t> & x, unsigned k, double result_critical) = 0;

};


/** clase abstracta que representa una prueba de discordancia univariada */
class UnivariateStatisticalTest
{
   public:
      // realiza una prueba estadistica univariada para tamaño de muestra n y k valores discordantes, retornando el valor critico para la muestra x
      virtual double test(std::vector<element_t>& x, unsigned k) = 0;

      // chequeo opcional previo a la evaluacion de la prueba e muestras contaminadas, retorna verdadero si la muestra pasa la prueba y debe ser evaluada por prueba, falso en caso contrario
      bool precheck_sample(unsigned n, unsigned k, std::vector<element_t>*x) {
        return true;
      }

      // realiza una prueba estadistica univariada para tamaño de muestra n y k valores discordantes, retornando el valor critico para la muestra x
      double test_contamined(std::vector<element_t>& x, unsigned k, ContaminedTestResult& r) {
        // double critical = std::numeric_limits<double>::max();
        double critical = test(x, k);
        r.process(x, k, critical);
        return critical;
      }
};


#endif
