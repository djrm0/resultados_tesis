#ifndef TN16_H
#define TN16_H

#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <cmath>


#include "../UnivariateStatisticalTest.hpp"
#include "../utils.hpp"


/*  Implementacion de la prueba estadistica Tietjen-Moore (1979) Tn16 */
class Tn16Ek: public UnivariateStatisticalTest
{
  public:
     double test(unsigned n, unsigned k, std::vector<element_t>*x, bool precheck = false) {
       double meanX = mean(x->begin(), x->end());
       // llenamos el arreglo de valores r (residuales absolutos)
       std::vector<double> r (n, 0.0);
       for (int i = 0; i < n; ++i) { r[i] = fabs(x->at(i) - meanX); }

       // debemos ordenar los valores de r en forma indirecta dado que necesitamos ordenar el arreglo con respecto a los valores en r, x ordenado en base a los indices obtenidos aqui forman el arreglo z descrito en tietjen-moore
       std::sort(x->begin(), x->end(),
                 [&r](std::size_t left, std::size_t right) {
                   return r[left] < r[right];
                 });
       
       double meanZ = 0.0;
       double meanZminusK = 0.0;
       for (int i = 0; i < x->size(); ++i) {
         meanZ += x->at(i);
         if (i < k) meanZminusK += x->at(i);
       }
       meanZ /= x->size();
       meanZminusK /= x->size() - k;

       // obtenemos el numerador y denominador
       double numerator = 0.0;
       double denominator = 0.0;
       for (int i = 0; i < n; i++) {
         denominator += pow(x->at(i) - meanZ, 2);
         if (i < n - k) numerator += pow(x->at(i) - meanZminusK, 2);
       }

       return numerator / denominator;
     };
};


bool descendent (const element_t &i, const element_t &j) { return (i > j); }


// verifica si los valores contaminantes se encuentran en alguno de los extremos. el extremo es determinado por lo, si lo es verdadero entonces checamos el extremo inferior, si lo es false checamos el extremo superior
bool tn16Lk_precheck_sample(unsigned n, unsigned k, std::vector<element_t>*y, bool lo = false) {
  bool valid = true;
  if (lo) {
    for (int i = 0; i < y->size(); i++) {
      // std::cout << i << ' ' << y->at(i).contamined << std::endl;
      if (i < k && !y->at(i).contamined) { valid = false; break; }
    }
  } else {
    for (int i = 0; i < y->size(); i++) {
      if (i >= y->size() - k && !y->at(i).contamined) { valid = false; break; }
    }
  }
  return valid;
}


double tn16Lk(unsigned n, unsigned k, std::vector<element_t>*y, bool lo = false, bool precheck = false) {

  if (lo) { std::sort(y->begin(), y->end(), &descendent); }
  else { std::sort(y->begin(), y->end()); }

  for (int i = 0; i < y->size(); i++) {
    std::cout << y->at(i) << " " << y->at(i).contamined << std::endl;
  }

  if (precheck) {
    if (!tn16Lk_precheck_sample(n, k, y, lo)) {
      return std::numeric_limits<double>::max();
    }
  }

  double meanY = mean(y->begin(), y->end());
  // media de los primeros n - k elementos de r
  double meanYminusK = mean(y->begin(), y->end() - k);

  // obtenemos el numerador
  double numerator = 0.0;
  for (int i = 0; i < n - k; i++) {
    numerator += pow(y->at(i) - meanYminusK, 2);
  }
  // obtenemos el denominador
  double denominator = 0.0;
  for (int i = 0; i < n; i++) {
    denominator += pow(y->at(i) - meanY, 2);
  }

  return numerator / denominator;
}



class Tn16LkUpper: public UnivariateStatisticalTest
{
  public:
     double test(unsigned n, unsigned k, std::vector<element_t>*y, bool precheck = false) {
       return tn16Lk(n, k, y, false, precheck);
     };

     bool precheck_sample(unsigned n, unsigned k, std::vector<element_t>*y) {
       return tn16Lk_precheck_sample(n, k, y);
     }
};

class Tn16LkLower: public UnivariateStatisticalTest
{
  public:
     double test(unsigned n, unsigned k, std::vector<element_t>*y, bool precheck = false) {
       return tn16Lk(n, k, y, true, precheck);
     };

     bool precheck_sample(unsigned n, unsigned k, std::vector<element_t>*y) {
       return tn16Lk_precheck_sample(n, k, y, true);
     }
};


#endif
