/**
 * Author: Daniel J. Ramirez <djrmuv@gmail.com>
 */
#ifndef STATISTICAL_TEST_SAMPLER_H
#define STATISTICAL_TEST_SAMPLER_H

#include <random>
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <future>
#include <thread>

#include <stdlib.h>
#include <math.h>

#include "../lib/ziggurat.h"

#include "UnivariateStatisticalTest.hpp"
#include "element_t.hpp"


/**  */
class StatisticalTestSampler {
public:
  template <typename Sseq>
  StatisticalTestSampler(
    UnivariateStatisticalTest *test,
    Sseq& seed1, Sseq& seed2,
    unsigned threads, unsigned sleep);
  /** Sin tiempo de espera entre evaluacion */
  template <typename Sseq>
  StatisticalTestSampler(
    UnivariateStatisticalTest *test,
    Sseq& seed1, Sseq& seed2,
    unsigned threads);
  /** Modo single thread */
  template <typename Sseq>
  StatisticalTestSampler(
    UnivariateStatisticalTest *test,
    Sseq& seed1, Sseq& seed2);
  ~StatisticalTestSampler();


  /** Cambia la semilla para el generador de valores pseudo aleatorios */
  template <typename Sseq>
  void seed(Sseq &seed1, Sseq &seed2);
  /** Cambia la semilla para el segundo generador de valores aleatorios */
  template <typename Sseq>
  void seed2(Sseq &seed1, Sseq &seed2);

  /** Realiza la evaluacion de la prueba estadistica */
  double test(std::vector<element_t>& x, unsigned k);

  /** */
  double test_from_random_contamined_sample(unsigned n, unsigned k, double delta = 0.0, unsigned thread = 0);

  double test_from_random_sample(unsigned n, unsigned k, unsigned thread = 0);

  // double test_from_random_contamined_sample(unsigned n, unsigned k, double delta, int contaminant_qty, unsigned thread = 0);

  /** realiza la prueba estadistica de forma repetida "observations" cantidad de vaces, para un tamaño de muestra n y k valores criticos */
  std::vector<double>* repeat_test(unsigned n, unsigned k, unsigned observations);


  /** Funcion de donde se obtienen todos los valores pseudoaleatorios */
  double nextRandom(bool use_distrib_2 = false);


  /** Metodo de marsaglia para retornar variables normales estandar
    * media = 0, desviacion estandar = 1
    */
  double marsaglia(bool use_distrib_2 = false);


  ContaminedTestResult& test_results;


private:
  std::uniform_real_distribution<double> distrib;
  /** Generadores de numeros aleatorios (Mersenne-twister), son necesarios
    * los dos para el metodo de Marsaglia */
  std::mt19937_64 g1;
  std::mt19937_64 g2;

  // usados para la generacion de muestras contaminadas
  std::uniform_real_distribution<double> distrib2;
  std::mt19937_64 g3;
  std::mt19937_64 g4;

  // prueba estadistica
  std::shared_ptr<UnivariateStatisticalTest> statistical_test;
  // UnivariateStatisticalTest& statistical_test;

  unsigned max_threads;
  unsigned sleep = 0;  // el tiempo de espera en millisegundos (obsoleto)

  bool return_first = true;  // usado para el metodo de marsaglia

  std::vector<std::vector<element_t>> m_data;


  /** Genera una muestra con k valores contaminados */
  void contamined_sample(unsigned n, unsigned k, double delta = 0.0, unsigned thread = 0);


  /** esta funcion genera valores criticos usando como parametros de la prueba
   *  a n y k, llenando las posiciones de start a end en el arreglo criticals */
  void sub_repeat_test(std::vector<double>& criticals, unsigned k,
                       unsigned start, unsigned end, unsigned thread);
};



/*****************************************************************************/
/* IMPLEMENTACION */
/*****************************************************************************/



template <typename Sseq>
StatisticalTestSampler::
StatisticalTestSampler(UnivariateStatisticalTest *test, Sseq& seed1, Sseq& seed2, unsigned threads, unsigned sleep) {
  statistical_test = test;
  distrib = std::uniform_real_distribution<double>(0.0, 1.0);
  distrib2 = std::uniform_real_distribution<double>(0.0, 1.0);
  g1 = std::mt19937_64(seed1);
  g2 = std::mt19937_64(seed2);
  g3 = std::mt19937_64(seed2);
  g4 = std::mt19937_64(seed1);
  max_threads = threads;
  sleep = sleep;

  m_data = std::vector<std::vector<element_t>>(max_threads);
  for (int i = 0; i < max_threads; i++) {
    m_data[i] = std::vector<element_t>();
  }
}


template <typename Sseq>
StatisticalTestSampler::
StatisticalTestSampler(UnivariateStatisticalTest *test, Sseq& seed1, Sseq& seed2, unsigned threads) : StatisticalTestSampler(test, seed1, seed2, threads, 0) {}


template <typename Sseq>
StatisticalTestSampler::
StatisticalTestSampler(UnivariateStatisticalTest *test, Sseq& seed1, Sseq& seed2) : StatisticalTestSampler(test, seed1, seed2, 1, 0) {}


StatisticalTestSampler::
~StatisticalTestSampler() {}


template <typename Sseq>
void StatisticalTestSampler::
seed(Sseq& s1, Sseq& s2) {
  g1.seed(s1);
  g2.seed(s2);
}

template <typename Sseq>
void StatisticalTestSampler::
seed2(Sseq& s1, Sseq& s2) {
  g3.seed(s1);
  g4.seed(s2);
}


void StatisticalTestSampler::
contamined_sample(unsigned n, unsigned k, double delta, unsigned thread) {
  delta = delta > 0 ? delta : 0.0;

  if (m_data[thread].size() != n) { m_data[thread].resize(n, 0.0); }

  // valores no contaminados
  for (int i = 0; i < n - k; i++) {
    m_data[thread][i] = nextRandom();
    m_data[thread][i].contamined = false;
  }
  for (int i = n - k; i < n; i++) {
    element_t v = nextRandom(true) + delta;
    v.contamined = true;
    m_data[thread][i] = v;
  }
}


double StatisticalTestSampler::
test(std::vector<element_t>& x, unsigned int k) {
  return statistical_test.test(x, k);
}


double StatisticalTestSampler::
test_from_random_contamined_sample(unsigned n, unsigned k, double delta, unsigned thread) {
  contamined_sample(n, k, delta, thread);

  return statistical_test.test_contamined(&m_data[thread], k, test_results);
}


double StatisticalTestSampler::
test_from_random_sample(unsigned n, unsigned k, unsigned thread) {
  // llenamos el arreglo con n valores aleatorios obtenidos de nuestra
  // distribucion, y nuestro generador de numeros aleatorios, esta sera
  // la poblacion de que evaluaremos en la prueba estadistica.
  if (m_data[thread].size() != n) {
    m_data[thread].resize(n , 0.0);
  }
  // std::vector<double> data (n, 0.0);
  for (int i = 0; i < n; i++) {
    m_data[thread][i] = nextRandom();
  }
  return test(&m_data[thread], k);
}


std::vector<double>* StatisticalTestSampler::
repeat_test(unsigned int n, unsigned int k, unsigned observations) {
  /** Aqui se almacena el resultado de las c observaciones */
  std::vector<double> *criticals = new std::vector<double>(observations, 0.0);

  // redimensiona los vectores de datos miembro
  for (int i = 0; i < max_threads; i++) {
    if (m_data[i].size() != n) {
      m_data[i].resize(n, 0.0);
    }
  }

  if (max_threads == 1) {
    sub_repeat_test(n, k, criticals, 0, observations, 0);

    return criticals;
  }
  // esto es en caso de que se use la implementacion multithread
  std::vector<std::thread> threads (max_threads);
  unsigned f = observations / max_threads;

  unsigned global_index = 0;
  // la cantidad de observaciones que realizara cada thread
  unsigned thread_observations = f;
  for (int i = 0; i < max_threads; i++) {
    // al ultimo agremas el residuo de la division
    if (i == max_threads - 1) {
      thread_observations += observations % max_threads;
    }
    threads[i] = std::thread(&StatisticalTestSampler::sub_repeat_test, this, n, k, criticals, global_index, thread_observations, i);
    global_index = thread_observations;
    thread_observations = global_index + f;
  }
  for (int i = 0; i < max_threads; i++) threads[i].join();

  return criticals;
}




void StatisticalTestSampler::
sub_repeat_test(unsigned n, unsigned k, std::vector<double>*criticals,
            unsigned start, unsigned end, unsigned thread) {
  for (int l = start; l < end; l++) {
    criticals->at(l) = test_from_random_sample(n, k, thread);
  }
}


/** Configure esta funcion para retornar los valores aleatorios */
double StatisticalTestSampler::
nextRandom(bool use_distrib_2) {
    return marsaglia(use_distrib_2);
}

/** Metodo de marsaglia para retornar dos valores normales independientes */
double StatisticalTestSampler::
marsaglia(bool use_distrib_2) {
    double u1, u2, w;
    do {
        if (use_distrib_2) {
          u1 = distrib(g3) * 2 - 1;
          u2 = distrib(g4) * 2 - 1;
        }
        else {
          u1 = distrib(g1) * 2 - 1;
          u2 = distrib(g2) * 2 - 1;
        }
        w = u1 * u1 + u2 * u2;
    } while (w > 1);

    double y = sqrt((-2 * log(w)) / w);

    if (return_first) {
        return_first = false;
        return u1 * y;
    }
    else {
        return_first = true;
        return u2 * y;
    }
}

#endif
