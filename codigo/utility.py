# -*- coding: utf-8 -*-
#
# Utilidades para los scripts en python, contiene funciones de redonde y parseo de cadenas de intervalos
#
# author: Daniel J. Ramirez

import sys
import glob
import os
from decimal import Decimal as D


def get_ns(increments):
    """ Parsea una cadena de incrementos, en un arreglo de valores, la
    sintaxis de la cadena de incrementos es la siguiente
    numero-incrementos otro_numero-incrementos ... numero_final
    por ejemplo si queremos los numeros 3,4,5,6,7,8,9,10,12,14,16,18,20
    usamos la suiguiente cadena:
    3-1 10-2 20

    """

    p = increments.split(' ')
    intervals = [int(i.split('-')[0]) for i in p]
    increments = [int(i.split('-')[1]) for i in p[:-1]]

    ns = []  # este es el arreglo de los n's para los cuales se generaran
             # valores criticos
    for i in range(len(intervals) - 1):
        for j in range(intervals[i], intervals[i + 1], increments[i]):
            ns.append(j)
    # este es el ultimo n no considerado en los ciclos anteriores
    ns.append(intervals[-1])

    return ns


def format_float(value):
    value = float(value)
    return format(value, '.16f');


def fraccion(valor):
    valor = str(valor)
    punto = valor.find('.')
    if punto < 0:
        return 0
    fraccion = valor[punto:]
    return float(fraccion)


def redondeo_dispersion_fraccionaria(dispersion):
    """ Redondeamos un parametro de dispersion fraccionario """

    str_dispersion = format_float(float(dispersion))

    point_index = str_dispersion.find('.')
    if point_index < 0:
        raise ValueError('El valor especificado no contiene punto decimal')

    # cantidad de digitos que tendra el numero final, sin contar los digitos
    # a la izquierda del punto decimal, y el punto decimal mismo.
    # deacuerdo con la regla 4 S.P. Verma  (Cap 3)
    # en caso de > .45 tomamos 2 digitos significativos, en otro caso 3
    digitos = 2 if float('.' + str_dispersion.strip('0.')) > .45 else 3

    for char in str_dispersion[point_index + 1:]:
        if char == '0':
            digitos += 1
        else:
            break

    fraccion = format(round(float(str_dispersion), digitos), '.%df' % digitos)
    return fraccion


def redondeo_fraccionario(media, dispersion):
    """ Redondeamos una medida de tendencia central en base a un parametro
    de dispersion

    """

    dispersion = redondeo_dispersion_fraccionaria(dispersion)
    digitos = len(dispersion.split('.')[1])
    media = format(round(float(media), digitos), '.%df'%digitos)

    if len(media) != len(dispersion):
        print('no coincenden las longitudes de la media y dispersion')

    return media, dispersion
