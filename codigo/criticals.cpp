#include <vector>
#include <iostream>
#include <fstream>
#include <utility>
#include <random>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>

#include "src/element_t.hpp"
#include "src/StatisticalTestSampler.hpp"
#include "src/UnivariateStatisticalTest.hpp"
#include "src/tests/Tn16.hpp"


#define STUDENT_T_CL_99_200_TWO_SIDED 2.6006393



/** genera un valores criticos para la prueba estadistica tn16, este
 *  programa requiere al menos 3 argumentos, los cuales
 *  son <cantidad de datos> <cantidad de observaciones> <directorio de salida>
 */
int main(int argc, char **argv) {
  using std::cout;
  using std::endl;

  const unsigned int CORES = std::thread::hardware_concurrency();
  // const unsigned int CORES = 1;

  if (argc < 4) {
    cout << "uso: criticals <n> <obs> <dir>" << endl;
    return 1;
  }
  const unsigned int N = atoi(argv[1]);
  const unsigned int OBS = atoi(argv[2]);
  const std::string DIR = argv[3];

  const double ALPHAS[] = {
      .9,   // 10 %
      .8,   // 20 %
      .7,   // 30 %
      .6,   // 40 %
      .5,   // 50 %
      .4,   // 60 %
      .3,   // 70 %
      .2,   // 80 %
      .1,   // 90 %
      .05,  // 95 %
      .025, // 97.5 %
      .02,  // 98 %
      .01,  // 99 %
      .005, // 99.5 %
      .002, // 99.8 %
      .001  // 99.9 %
  };
  const int ALPHAS_COUNT = 16;

  // reestringimos el numero maximo de outliers a 50
  int max_outliers = N / 2;
  if (max_outliers > 50) {
    max_outliers = 50;
  }

  const long SEEDS[20][4] = {
    {0x34500, 0x456, 0x67800, 0x567},
    {0x680, 0x888, 0x1111, 0x341},
    {0x999,0xaaa,0x1,0xa},
    {0x3,0xa,0xb,0xc},
    {0x01,0x2,0x1,0x1},
    {0xea857ccd, 0x4cc1d30f, 0x8891a8a1, 0xa6b7aadb},
    {0x512c0c03, 0x5a9ad5d9,0x885d05f5, 0x4e20cd47},
    {0x76,0x111,0xaaa,0x885d05f5},
    {0xeee,0x3000,0x885d05f5,0xababa},
    {0x4ebc98,0x1,0xffff,0x2da87693},
    {0x95f24dab, 0x0b685215, 0xe76ccae7, 0xaf3ec239},
    {0x512c0c03, 0xea857ccd, 0x4cc1d30f, 0x8891a8a1},
    {0xfffabc, 0xbcdfff, 0xfffdef, 0xcde000},
    {0x345, 0x456, 0x678, 0x56dd7},
    {0xabc, 0xffbcd, 0xdefff, 0xcde},
    {0x95f24dab,0x24a590ad, 0xc1de75b7,0x8121da71},
    {0x512c0c03,0x0b685215,0x69e4b5ef,0x8858a9c9},
    {0x8b823ecb,0xea857ccd,0xe76ccae7,0xbf456141},
    {0x2da87693,0x885d05f5, 0x4cc1d30f,0x8891a8a1},
    {0x715fad23,0xa7bdf825,0xffdc8a9f,0x5a9ad5d9}
  };
  const unsigned SEEDS_COUNT = 20;

  std::vector<std::array<std::array<unsigned int, 4>, 2>> SEED_COMBINATIONS;
  std::vector<bool> used_seeds (SEEDS_COUNT, false);

  for (int i = 0; i < SEEDS_COUNT; i++) {
      used_seeds[i] = true;
      for (int j = 0; j < SEEDS_COUNT; j++) {
          if (used_seeds[j]) continue;

          std::array<unsigned int, 4> s1;
          std::array<unsigned int, 4> s2;
          for (int k = 0; k < 4; ++k) {
              s1[k] = SEEDS[i][k];
              s2[k] = SEEDS[j][k];
          }
          std::array<std::array<unsigned,4>,2> seed_pair;
          seed_pair[0] = s1;
          seed_pair[1] = s2;
          SEED_COMBINATIONS.push_back(seed_pair);
      }
  }
  cout << SEED_COMBINATIONS.size() << " seed combinations " << endl;

  // aqui almacenaremos los valores criticos parciales, una vez que
  // tengamos los valores para todos las combinaciones de semillas
  // podremos mezclar los resultados
  double results[ALPHAS_COUNT][max_outliers][SEED_COMBINATIONS.size()];

  // instanciacion de la clase para nuestra prueba estadistica, en este caso Ek
  Tn16Ek ek_test;
  int s1, s2;
  StatisticalTestSampler sampler(&ek_test, s1, s2, CORES);

  cout << "Using " << CORES << " cores." << endl;

  for (int i = 0; i < SEED_COMBINATIONS.size(); ++i) {
    auto seed_pair = SEED_COMBINATIONS[i];
    std::seed_seq seed1 (seed_pair[0].begin(), seed_pair[0].end());
    std::seed_seq seed2 (seed_pair[1].begin(), seed_pair[1].end());
    sampler.seed(seed1, seed2);

    for (int k = 0; k < max_outliers; k++) {
      std::vector<double>* criticals = sampler.repeat_test(N, k + 1, OBS);
      std::sort(criticals->begin(), criticals->end());

      // obtenemos los valores criticos para los cortes especificados en
      // ALPHAS
      for (int alpha_index = 0; alpha_index < ALPHAS_COUNT; alpha_index++) {
        unsigned index = (unsigned)(ALPHAS[alpha_index] * (double)OBS);
        double critical = criticals->at(index);

        results[alpha_index][k][i] = critical;
      }
      delete criticals;
    }
  }

  for (int alpha = 0; alpha < ALPHAS_COUNT; alpha++) {
      std::string file_name_data = "N_" + std::to_string(N) + "-" + "alfa_" + std::to_string(ALPHAS[alpha]) + ".txt";
      std::string new_dir = DIR + "/" + std::to_string(N);

      mkdir((new_dir).c_str(), 0777);
      mkdir((new_dir + "/error").c_str(), 0777);
      mkdir((new_dir + "/error_estandar").c_str(), 0777);

      std::ofstream criticals_file;
      std::ofstream errors_file;
      std::ofstream se_file;

      criticals_file.open(new_dir + "/criticos_" + file_name_data);
      errors_file.open(new_dir + "/error/errores_" + file_name_data);
      se_file.open(new_dir + "/error_estandar/se_" + file_name_data);
      for (int k = 0; k < max_outliers; k++) {
          double mean_critical = 0.0;
          for (int i = 0; i < SEED_COMBINATIONS.size(); i++) {
              mean_critical += results[alpha][k][i];
          }
          mean_critical /= (double)SEED_COMBINATIONS.size();
          criticals_file << mean_critical << ", ";

          // desviacion estandar
          double sd = 0.0;
          for (int i = 0; i < SEED_COMBINATIONS.size(); i++) {
              sd += pow((results[alpha][k][i] - mean_critical), 2.0);
          }
          sd = sqrt(sd / (double)SEED_COMBINATIONS.size());
          // error standar
          double se = sd / sqrt((double)SEED_COMBINATIONS.size());
          se_file << se << ", ";
          double tstudent_99_200_twosidedCL = 2.6006393;
          se *= tstudent_99_200_twosidedCL;
          errors_file << se << ", ";
      }
      criticals_file << endl;
      criticals_file.close();
      se_file << endl;
      se_file.close();
      errors_file << endl;
      errors_file.close();
  }

  return 0;
}
