#include <random>
#include <iostream>

#include "../src/tests/Tn16.hpp"
#include "../src/element_t.hpp"
#include "../src/StatisticalTestSampler.hpp"
#include "../lib/ziggurat.hpp"


int main() {
  Tn16Ek ek;
  Tn16LkLower lk_lower;
  Tn16LkUpper lk_upper;

  double values[] = {-1.40, -.44, -.30, -.24, -.22, -.13, -.05, .06, .10, .18, .20, .39, .48, .63, 1.01};
  std::vector<element_t> v (values, values + sizeof(values) / sizeof(double) );

  // usando el metodo en la clase Tn16Ek
  double c = ek.test(v.size(), 2, &v); //    (1)
  std::cout << c << '\n';

  // creando un sampler con la clase Tn16Ek
  std::seed_seq seed1 = {2383, 1193};
  std::seed_seq seed2 = {1234, 1231};
  // std::cout << typeid(seed1) << std::endl;

  // Ziggurat z;
  // StatisticalTestSampler sampler (&ek, seed1, seed2, 2);
  // for (int i = 0; i < 10000; i++) {
  //   r4_nor();
  //   // z.randExc();
  // }
}
