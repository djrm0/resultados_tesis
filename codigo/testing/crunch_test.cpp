#include <random>
#include <iostream>

#include "../src/tests/Tn16.hpp"
#include "../src/element_t.hpp"
#include "../src/StatisticalTestSampler.hpp"


int main() {
  Tn16Ek ek;
  std::seed_seq seed1 = {2383, 1193};
  std::seed_seq seed2 = {1234, 1231};

  StatisticalTestSampler sampler (&ek, seed1, seed2, 2);
  c = sampler.test(v.size(), 2, &v);  // hace los mismo que (1)
  std::cout << c << std::endl;
}
