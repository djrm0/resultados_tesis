#include <iostream>
#include <vector>
#include <algorithm>

#include "../src/element_t.hpp"


double add(double s, double s2) {
  return s + s2;
}

int main() {
  element_t dd;
  element_t d2 = 2.0;
  dd.contamined = true;
  d2.contamined = true;
  dd = 5.1;
  dd += 1.0;
  dd += d2;
  element_t ff = dd;
  ff += 100;
  ff.contamined = true;
  double k = ff;
  std::cout << dd << " " << dd.contamined << std::endl;
  std::cout << ff << " " << ff.contamined << std::endl;
  std::cout << k << std::endl;
  std::cout << " --- - " << sizeof(ff) << std::endl;

  std::cout << add(ff, 5.0) << std::endl;

  std::vector<element_t> mmm (5);
  mmm[0] = ff;
  std::cout << mmm[0] << " C " << (mmm[0]).contamined << std::endl;


  std::vector<element_t> elements (20, 0.0);
  elements[0].contamined = true;
  elements[1] = elements[0];
  // elements[2].contamined = true;
  // elements[3].contamined = true;
  for (int i = 0; i < elements.size(); i++) {
    elements[i] = 100 - i;
  }
  std::cout << std::endl;
  for (int i = 0; i < elements.size(); i++) {
    std::cout << elements[i] << " " << elements[i].contamined << std::endl;
  }
  std::cout << std::endl;
  std::sort(elements.begin(), elements.end());
  for (int i = 0; i < elements.size(); i++) {
    std::cout << elements[i] << " " << elements[i].contamined << std::endl;
  }

  // std::vector<element_t> *elements2 new std::vector<element_t>(5, 0.0);
  // elements2[0].contamined = true;
  // elements2[1].contamined = true;
  // for (int i = 0; i < elements2.size(); i++) {
  //   elements2[i] = 10 - i;
  // }
  // std::sort(elements2.begin(), elements2.end());
  // for (int i = 0; i < elements2.size(); i++) {
  //   std::cout << elements2[i].contamined << std::endl;
  // }
}
