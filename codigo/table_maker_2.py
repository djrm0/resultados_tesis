#
#
#

import sys
import glob
import os
from decimal import Decimal as D
from utility import *
from dataminer import *



if len(sys.argv) < 2:
    print('uso: table_maker_2.py <directorio_base>')
    exit()

base_dir = sys.argv[1]
if not os.path.isdir(base_dir):
    print('el directorio especificado no existe')
    exit()

base_subdirs = ['', 'error/', 'error_estandar/']


# DATA contiene todos los valores crudos en forma de cadena, para la media
# el error estandar y la incertidumbre
#
# DATA esta organizado de la siguiente forma
#
# DATA[alfa][N][tipo][k]
#
# donde alfa es el nivel de significancia,
# N es el tamaño de la muestra
# tipo es un indice en [0,1,2], que representa el tipo de dato, 0 indica que
# el arreglo en esa posicion contiene las medias, el 1 indica que el arreglo en
# esa posicion contiene las incertidumbres, y el 2 indica que el arreglo en
# en esa posicion contiene los errores estandares.
#
# Por ejemplo si quisieramos obtener el valor critico, la incertidumbre y el
# error estandar para algun alfa y algun N y algun valor de K obtendriamos
#
# media = DATA[alfa][N][0][K]
# incertidumbre = DATA[alfa][N][1][K]
# error_estandar = DATA[alfa][N][2][K]
DATA, ns = getData(base_dir)
print (DATA)

ns = get_ns('3-1 50-5 80-10 100')




def simple_csv_table(table_file_name, alfa, data_index):
    table_str = ''

    for n in ns:
        if not str(n) in DATA[alfa].keys():
            continue

        table_str += str(n) + ',' if n != ns[-1] else str(n)
        n_data = DATA[alfa][str(n)]
        for i in range(0, len(n_data[0])):
            value = n_data[data_index][i]
            # media, se = redondeo_fraccionario(n_data[0][i], n_data[2][i])
            table_str += value
            if i < len(n_data[0]) - 1:
                table_str += ','
        table_str += '\n'

    table_file = open(base_dir + '../tablas/' + table_file_name, 'w')
    table_file.write(table_str)
    table_file.close()


def csv_table(table_file_name):
    # creamos la tabla de medias y errores estandar
    table_str = ''

    # latex_format = latex_table(DATA)

    for n in ns:
        if not str(n) in DATA[alfa].keys():
            continue

        table_str += str(n) + ','
        n_data = DATA[alfa][str(n)]
        for i in range(0, len(n_data[0])):
            media, se = redondeo_fraccionario(n_data[0][i], n_data[2][i])
            table_str += media + ' ± ' + se
            if i < len(n_data[0]) - 1:
                table_str += ','
        table_str += '\n'

    table_file = open(tables_dir + table_file_name, 'w')
    table_file.write(table_str)
    table_file.close()



def latex_table(_data, _ns, _ks, data_index, alfa, table_name, short_table_name, include_name):
    table = ''
    table = '\\begin{table}[H]\n'
    if include_name:
        table += '\\caption[%s]{%s}\n' % (short_table_name, table_name)
    table += '\\begin{tabular}{' + 'l ' * (len(_ks) + 1) + '}\n'
    # table += '\\hline\n'
    # table += '\\multicolumn{%d}{c}{%s $\\alpha = %s$}\\\\\n' % (len(_ks) + 1, table_name, alfa.rstrip('0'))
    # table += '\\hline\n'
    table += 'n & '
    for k in _ks:
        table += '$k = %d$ ' % k
        if k < _ks[-1]:
            table += ' & '
    table += '\\\\\n\\hline\n'

    for n in _ns:
        if int(n / 2) < _ks[0]:
            continue

        table += '%d & ' % n
        for k in _ks:
            if k > len(_data[str(n)][data_index]):
                continue
            value = _data[str(n)][data_index][k-1]
            if data_index == 0:
                incertidumbre = _data[str(n)][1][k-1]
                value, incertidumbre = redondeo_fraccionario(value, incertidumbre)
            else:
                value = redondeo_dispersion_fraccionaria(value)
            table += str(value)
            # if k < len(_data[str(n)][data_index]):
            if k < _ks[-1]:
                table += ' & '
        table += '\\\\\n'
    table += '\\end{tabular}\n'
    table += '\\end{table}\n'

    return table
import math


def latex_table_dir(name, data_index, table_name, out_dir):
    dir_prefixes = ['media', 'incertidumbre', 'error_estandar']

    if not out_dir:
        out_dir = './'
    out_dir += '/tablas/' if out_dir[-1] != '/' else 'tablas/'

    table_file_format = """
\\documentclass[principal.tex]{{subfiles}}
\\begin{{document}}
{0}
\\end{{document}}
    """


    max_k = 10
    cuts = [21, 39, 100]
    cuts = [100]
    cuts_index = []
    for cut in cuts:
        cut_index = ns.index(cut)
        if cut_index > 0:
            cuts_index.append(cut_index)

    for alfa in ALFAS:
        _data = DATA[alfa]

        _alfa = alfa.rstrip('0')
        conf = (100 - float(_alfa) * 100)
        if not conf % 1 > 0:
            conf = int(conf)
        conf = str(conf)
        _table_name = '{0} con $n = 3(1)\\ 50(5)\\ 80(10)\\ 100$ $\\alpha = {1}$ (nivel de confianza {2}\%)'.format(table_name, _alfa, conf)
        short_table_name = '{0} nivel de confianza {1}\%'.format(table_name, conf)

        tables_dir = out_dir + '%s/' % (conf)
        print(tables_dir)
        if not os.path.isdir(tables_dir):
            os.makedirs(tables_dir)



        start = 0
        for cut_index in cuts_index:
            end = cut_index + 1
            _ns = ns[start:end]
            k_cuts = []
            mk = int(_ns[-1] / 2)
            for i in range(1, math.ceil(mk / max_k) + 1):
                k_cuts.append(min(i * max_k, mk))

            table_file_name = '%s.tex' % (dir_prefixes[data_index])
            tables_file_contents = ''
            main_table_file_content = ''

            k_start = 1
            for k_cut in k_cuts:
                k_end = k_cut + 1
                _ks = [i for i in range(k_start, k_end)]

                # table_file_name = 'k_%d-%d.tex' % (k_start, k_end - 1)

                k_start = k_end
                # aqui ya tenemos una tabla LaTeX con los parametros especificados
                table = latex_table(_data, _ns, _ks, data_index, alfa, _table_name, short_table_name, k_cut == k_cuts[0])

                tables_file_contents += table

                # table_file_content = table_file_format.format(table)
            tables_file_contents = table_file_format.format( tables_file_contents, _table_name)
            table_file = open(tables_dir + table_file_name, 'w')
            table_file.write(tables_file_contents)
            table_file.close()
            start = end


def latex_table_book(name, data_index, table_name):
    ''' '''

    table_prefixes = ['A', 'B', 'C']

    book_file_name = name + '.tex'

    tables = ''
    book_name = 'Tablas de %s $n = 3(1)\\ 50(5)\\ 80(10)\\ 100$' % table_name
    tables += '\n\\large{%s}\\small\n' % book_name
    if data_index == 1:
        tables += '\n\\vspace{.1cm}\n'
        tables += '\nError estandar multiplicado por el coeficiente t Student $t^{cv}_{99ts}$ con 200 grados de libertad $(2.6006393)$'
    tables += '\n\\vspace{1cm}\n'
    tables += '\\renewcommand{\\thetable}{%s\\arabic{table}}\\setcounter{table}{0}'%table_prefixes[data_index]



    max_k = 10
    cuts = [21, 39, 100]
    cuts = [100]
    cuts_index = []
    for cut in cuts:
        cut_index = ns.index(cut)
        if cut_index > 0:
            cuts_index.append(cut_index)

    for alfa in ALFAS:
        _data = DATA[alfa]

        _alfa = alfa.rstrip('0')
        # print(_alfa)
        conf = (100 - float(_alfa) * 100)
        if not conf % 1 > 0:
            conf = int(conf)
        conf = str(conf)
        _table_name = '{0} $\\alpha = {1}$ (nivel de confianza {2}\%)'.format(table_name, _alfa, conf)
        # tables += '\n\\normalsize{%s}\\small\n' % _table_name


        start = 0
        for cut_index in cuts_index:
            end = cut_index + 1
            _ns = ns[start:end]
            k_cuts = []
            mk = int(_ns[-1] / 2)
            for i in range(1, math.ceil(mk / max_k) + 1):
                k_cuts.append(min(i * max_k, mk))
            k_start = 1
            for k_cut in k_cuts:
                k_end = k_cut + 1
                _ks = [i for i in range(k_start, k_end)]
                k_start = k_end
                table = latex_table(_data, _ns, _ks, data_index, alfa, _table_name)
                tables += '\n\\vspace{.6cm}\n'
                tables += table
            start = end
        tables += '\n\\vspace{1cm}\n'


    book_content = '\\documentclass[10pt]{article}'
    book_content += '\\usepackage[margin=1cm]{geometry}'
    book_content += '\\usepackage[utf8]{inputenc}\n\\usepackage[T1]{fontenc}'
    book_content += '\\usepackage[spanish]{babel}'
    # book_content += '\\usepackage{float}'
    book_content += '\\usepackage{floatrow}'
    book_content += '\\DeclareFloatFont{footnotesize}{\\footnotesize}'
    book_content += '\\floatsetup[table]{font=footnotesize}'
    book_content += '\\floatsetup[table]{capposition=top}'
    book_content += '\\usepackage{caption}\n\\captionsetup[table]{name=Tabla}'
    book_content += '\\begin{document}'
    book_content += '\\small'
    book_content += '\\thispagestyle{empty}\\pagestyle{empty}'
    book_content += tables
    book_content += '\\end{document}'

    book_file = open(book_file_name, 'w')
    book_file.write(book_content)
    book_file.close()

    outdir = '.'
    slash_index = name.rfind('/')
    if slash_index > 0:
        outdir = name[:slash_index]
    os.system('pdflatex -output-directory %s %s' % (outdir, book_file_name))
    # os.system('pdflatex -output-directory %s %s' % (outdir, book_file_name))
    os.remove(book_file_name)
    os.remove(name + '.aux')
    os.remove(name + '.log')



def html_table_book(name, data_index, table_name):
    ''' '''

    page_file_name = name + '.html'

    max_k = len(DATA[ALFAS[0]][str(ns[-1])][0])

    tables = ''
    for alfa in DATA.keys():
        table = '<table>'
        table += '<thead><tr><td>'
        table += '%s alfa = %s'%(table_name, alfa.rstrip('0'))
        table += '</td></tr></thread>'
        table += '<tbody>'
        table += '<tr style="border-bottom: 1px solid black">'
        table += '<td>n</td>'
        for i in range(1, max_k + 1):
            table += '<td>k = %d</td>' % i
        table += '</tr>'
        for n in ns:
            if not str(n) in DATA[alfa].keys():
                continue

            table += '<tr>'
            table += '<td>%d</td>' % n
            n_data = DATA[alfa][str(n)]
            for i in range(0, len(n_data[0])):
                media, se = redondeo_fraccionario(n_data[0][i], n_data[2][i])
                table += '<td>%s</td>' % media
            table += '</tr>'
        table += '</tbody>'
        table += '</table>'
        tables += table

    page_content = '<!doctype html><html>'
    page_content += '<head><meta coding="utf-8"/></head>'
    page_content += '<body>'
    page_content += tables
    page_content += '</body></html>'

    page_file = open(page_file_name, 'w')
    page_file.write(page_content)
    page_file.close()

# tables_dir = base_dir + 'tables/'
# if not os.path.isdir(tables_dir):
#     print('creando directorio para almacenar las tablas en: ' + tables_dir)
#     os.mkdir(tables_dir)
# for alfa in DATA.keys():
#     table_file_name = 'tabla-alfa_' + alfa + '.csv'

# latex_table_book('results/medias_Ek', 0, 'Valores Críticos para $E_k$, ')
# latex_table_book('results/errores_estandar_Ek', 2, 'Errores Estandar para $E_k$, ')
# latex_table_book('results/incertidumbre_Ek', 1, 'Incertidumbre para $E_k$, ')


# latex tables
latex_table_dir('tablas/L_k', 0, 'Valores críticos para $L_k$, ', '')
latex_table_dir('tablas/L_k', 1, 'Incertidumbre para $L_k$, ', '')
latex_table_dir('tablas/L_k', 2, 'Errores estandar para $L_k$, ', '')

# for alfa in ALFAS:
#     _alfa = alfa.rstrip('0')
#     conf = (100 - float(_alfa) * 100)
#     if not conf % 1 > 0:
#         conf = int(conf)
#     conf = str(conf).replace('.', '_')
#
#     simple_csv_table('media_%s.csv' % conf, alfa, 0)
#     simple_csv_table('incertidumbre_%s.csv' % conf, alfa, 1)



# html_table_book('results/tablas', [], 'Valores Críticos para Ek ')
