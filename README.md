TODOS LOS ARCHIVOS EN ESTE REPOSITORIO SON DEL DOMINIO PUBLICO, USTED PUEDE 
USARLOS COMO DESEE.


Resultados de la tesis de
Daniel Jaziel Ramirez Martinez

Generación de nuevos valores críticos para
la prueba estadística Tietjen-Moore
mediante simulación Monte Carlo.


El codigo fuente usado se encuentra en el directorio "codigo".

En resultados se encuentran todas las tablas en formato CSV y crudo, 
ademas de las ecuaciones de interpolacion y sus graficas.
